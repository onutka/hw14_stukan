<?php
namespace instance{
    function sayHello(){
        return __FUNCTION__;
    }
}

namespace my{
    function sayHello(){
        return __FUNCTION__;
    }

    echo \sayHello();
}

namespace {
    use my;
    function sayHello(){
        return __FUNCTION__;
    }
    echo '<br>';
    echo my\sayHello();
}

